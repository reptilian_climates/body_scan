

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	python -m unittest UT_STATUS.py

	python -m unittest *STATUS.py
'''

import pathlib
from os.path import dirname, join, normpath


import status_modules.structure_paths as structure_paths
structure_paths.add (structure_paths.find ())


def FIND_PATH_STATUS (PATHS, PATH_END):
	for PATH in PATHS:
		SPLIT = PATH ["PATH"].split (PATH_END)
	
		if (len (SPLIT) == 2 and len (SPLIT [1]) == 0):
			return PATH 

	print ("PATH_END:", PATH_END)
	raise Exception ("PATH NOT FOUND")


import BODY_SCAN

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		print ("test_1")

		import pathlib
		THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

		from os.path import dirname, join, normpath
		STASIS = normpath (join (THIS_FOLDER, "STASIS/2"))

		SCAN = BODY_SCAN.START (
			GLOB = STASIS + '/**/*HEALTH.py',
			RELATIVE_PATH = STASIS,
			MODULE_PATHS = [
				#* FIND_STRUCTURE_PATHS (),			
				normpath (join (STASIS, "MODULES"))
			]
		)
		STATUS = SCAN ['STATUS']
		PATHS = STATUS ["PATHS"]
		
		import json
		print ("UT 2 STATUS FOUND", json.dumps (STATUS ["STATS"], indent = 4))

		assert (len (PATHS) == 2)
		assert (STATUS ["STATS"]["ALARMS"] == 0)
		assert (STATUS ["STATS"]["EMPTY"] == 1)
		assert (STATUS ["STATS"]["CHECKS"]["PASSES"] == 2)
		assert (STATUS ["STATS"]["CHECKS"]["ALARMS"] == 1)
		
		PATH_1 = FIND_PATH_STATUS (PATHS, "PATH_1_HEALTH.py")
		assert (type (PATH_1) == dict)

