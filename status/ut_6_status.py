



'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	python -m unittest UT_6_STATUS.py

	python -m unittest *STATUS.py
'''

import pathlib
from os.path import dirname, join, normpath

import status_modules.structure_paths as structure_paths
structure_paths.add (structure_paths.find ())



import BODY_SCAN

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		import pathlib
		THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

		from os.path import dirname, join, normpath
		STASIS = normpath (join (THIS_FOLDER, "STASIS/6"))

		print ("SEARCHING:", STASIS)

		SCAN = BODY_SCAN.START (
			GLOB = STASIS + '/**/*HEALTH.py',
			
			SIMULTANEOUS = True,
			
			RELATIVE_PATH = STASIS,
			MODULE_PATHS = [
				#* FIND_STRUCTURE_PATHS (),			
				normpath (join (STASIS, "MODULES"))
			]
		)
		STATUS = SCAN ['STATUS']
		PATHS = STATUS ["PATHS"]
		
		import json
		print ("UT 6 STATUS FOUND", json.dumps (STATUS ["STATS"], indent = 4))
		assert (len (PATHS) == 3)
				
		assert (STATUS ["STATS"]["ALARMS"] == 1)
		assert (STATUS ["STATS"]["EMPTY"] == 1)
		assert (STATUS ["STATS"]["CHECKS"]["PASSES"] == 7)
		assert (STATUS ["STATS"]["CHECKS"]["ALARMS"] == 1)

