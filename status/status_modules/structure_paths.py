

import pathlib
from os.path import dirname, join, normpath

def find ():
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()
	STRUCTURE = normpath (join (THIS_FOLDER, "../../structure"))

	return [
		normpath (join (STRUCTURE, "modules")),
		normpath (join (STRUCTURE, "modules_pip"))
	]
	

def add (PATHS):
	import pathlib
	FIELD = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	for PATH in PATHS:
		sys.path.insert (0, normpath (join (FIELD, PATH)))