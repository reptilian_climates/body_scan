

'''
	This checks if a partial string
	is at the end of another string.
'''
def END_OF_STRING_IS (STRING, PARTIAL):
	try:
		INDEX = STRING.index (PARTIAL)

		if (len (STRING) == (INDEX + len (PARTIAL))):
			return True
			
	except Exception:
		pass

	return False

import MODULE_1

def CHECK_1 ():
	print ("__file__ CHECK", __file__)
	
	assert (
		END_OF_STRING_IS (__file__, "5/PATH_1_HEALTH.py") ==
		True
	)
	
	MODULE_1.START (END_OF_STRING_IS)

	return;


CHECKS = {
	"CAN ACCESS THE __file__ VARIABLE": CHECK_1
}