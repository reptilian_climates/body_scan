



def CHECK_1 ():
	print ("CHECK 1")


def CHECK_2 ():
	print ("CHECK 2")
	raise Exception ("NOT 100%")


CHECKS = {
	"CHECK 1": CHECK_1,
	"CHECK 2": CHECK_2,
	"CHECK 3": lambda : (),
	"CHECK 4": lambda : (),
	"CHECK 5": lambda : (),
	"CHECK 6": lambda : (),
	"CHECK 7": lambda : (),
	"CHECK 8": lambda : ()
}