

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	(cd STATUS && python -m unittest UT_1_STATUS.py)

	(cd status && python -m unittest *status.py)
'''

import pathlib
from os.path import dirname, join, normpath


import status_modules.structure_paths as structure_paths
structure_paths.add (structure_paths.find ())


def FIND_PATH_STATUS (PATHS, PATH_END):
	for PATH in PATHS:
		SPLIT = PATH ["PATH"].split (PATH_END)
	
		if (len (SPLIT) == 2 and len (SPLIT [1]) == 0):
			return PATH 

	print ("PATH_END:", PATH_END)
	raise Exception ("PATH NOT FOUND")


import BODY_SCAN

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		print ("test_1")

		import pathlib
		from os.path import dirname, join, normpath
		this_folder = pathlib.Path (__file__).parent.resolve ()
		stasis = normpath (join (this_folder, "STASIS/1"))

		SCAN = BODY_SCAN.START (
			GLOB = stasis + '/**/*health.py',
			MODULE_PATHS = [
				normpath (join (stasis, "modules"))
			],
			RELATIVE_PATH = stasis
		)
		STATUS = SCAN ["STATUS"]
		PATHS = STATUS ["PATHS"]
		
		import json
		print ("UT 1 STATUS FOUND", json.dumps (STATUS ["STATS"], indent = 4))

		assert (len (PATHS) == 2)
		assert (STATUS ["STATS"]["CHECKS"]["PASSES"] == 4)
		assert (STATUS ["STATS"]["CHECKS"]["ALARMS"] == 1)
		
		PATH_1 = FIND_PATH_STATUS (PATHS, "path_1_health.py")
		assert (type (PATH_1) == dict)
		
		PATH_2 = FIND_PATH_STATUS (PATHS, "path_2_health.py")
		assert (type (PATH_2) == dict)